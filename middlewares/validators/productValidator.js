const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const { product } = require("../../db/models");
const validator = require("validator");

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    if (!validator.isNumeric(req.body.price)) {
      errors.push("Price must be a number");
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    req.body.directory = "product";
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await product.findOne({
      where: { id: req.params.id },
    });

    if (!findData) {
      errors.push("Product Not Found");
    }
    if (!validator.isNumeric(req.body.price)) {
      errors.push("Price must be a number");
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    req.body.directory = "product";

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let findData = await product.findOne({
      where: { id: req.params.id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Product Not Found",
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
