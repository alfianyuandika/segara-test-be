const path = require("path");
const crypto = require("crypto");
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");
const { getDefaultSettings } = require("http2");

const REGION = "ap-southeast-1";

const uploadParams = (directory, filename, body, mimetype) => {
  return {
    ACL: "public-read",
    Bucket: process.env.S3_BUCKET,
    Key: `/${directory}/${filename}`,
    Body: body,
    ContentType: mimetype,
  };
};

const s3 = new S3Client({
  region: REGION,
  credentials: {
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY,
  },
});

const run = async (directory, filename, body, mimetype) => {
  try {
    const data = await s3.send(
      new PutObjectCommand(uploadParams(directory, filename, body, mimetype))
    );
    return directory + "/" + filename;
  } catch (err) {}
};

exports.uploadImage = async (req, res, next) => {
  try {
    let errors = [];

    if (req.files) {
      const file = req.files.image;

      if (!file.mimetype.startsWith("image")) {
        errors.push("File must be an image");
      }

      if (errors.length > 0) {
        return res.status(400).json({
          message: errors.join(", "),
        });
      }

      if (file.size > 1000000) {
        errors.push("Image must be less than 1MB");
      }

      if (errors.length > 0) {
        return res.status(400).json({
          message: errors.join(", "),
        });
      }

      let fileName = crypto.randomBytes(16).toString("hex");

      file.name = `${fileName}${path.parse(file.name).ext}`;

      req.body.image = await run(
        req.body.directory + "/" + "images",
        file.name,
        file.data,
        file.mimetype
      );
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: e.message,
    });
  }
};
