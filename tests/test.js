const request = require("supertest");
// const jwt_decode = require("jwt-decode");
const app = require("../index");
const { product, user } = require("../db/models");
let tokenCustomer;

//////////////////////////////////////////////////////////////// User Sign Up / Sign In //////////////////////////////////////////////////////////////////
// Auth test
describe("Auth Test", () => {
  describe("/signup POST", () => {
    it("It should make user and get the token", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
  });
  describe("/signup POST", () => {
    it("It should error email has been used", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email has been used");
    });
  });
  
  
  describe("/signup POST", () => {
    it("It should error confirm password", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_12345",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password confirmation must be same as password"
      );
    });
  });
  describe("/signup POST", () => {
    it("It should error email format", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email field must be valid email");
    });
  });
  describe("/signup POST", () => {
    it("It should error weak password", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "1234",
        confirmPassword: "1234",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password needs (uppercase & lowercase characters, number, and symbol)"
      );
    });
  });
  describe("/signin POST", () => {
    it("It should success signin", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
  });
  describe("/signin POST", () => {
    it("It should error signin email not found", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "alfian12345@gmail.com",
        password: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email not found");
    });
  });
  describe("/signin POST", () => {
    it("It should error signin wrong password", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "alfian1234@gmail.com",
        password: "Alfian_12345",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Wrong password");
    });
  });
});

//////////////////////////////////////////////////////////////// CRUD Product /////////////////////////////////////
//Create Product
describe("Create Product Test", () => {
  describe("product/create POST", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .post("/product/create/")
          .send({
            name: "powerbank",
            price: 150000,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("product/create POST", () => {
    it("It should error price must be a number", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .post("/product/create/")
          .send({
            name: "powerbank",
            price: "150000abc",
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Price must be a number");
      }
    });
  });
  describe("product/create POST", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .post("/product/create/")
          .send({
            name: "powerbank",
            price: 150000,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("product/create POST", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app).post("/product/create/").send({
          name: "powerbank",
          price: 150000,
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Get All Product
describe("Get Product All Test", () => {
  describe("product/ GET", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .get("/product/")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("product/ GET", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .get("/product/")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("product/ GET", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .get("/product/")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Get One Product
describe("Get One Product Test", () => {
  describe("product/:id GET", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .get("/product/1")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("product/:id GET", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .get("/product/5")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Product Not Found");
      }
    });
  });
  describe("product/:id GET", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .get("/product/1")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("product/:id GET", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .get("/product/1")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Delete Product
describe("Delete Product Test", () => {
  describe("product/update/:id PATCH", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/product/update/1")
          .send({
            name: "Guitar",
            price: 1500000,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Delete Product Success");
      }
    });
  });
  describe("product/update/:id PATCH", () => {
    it("It should error product not found", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/product/update/6")
          .send({
            name: "Guitar",
            price: 1500000,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Product Not Found");
      }
    });
  });
  describe("product/update/:id PATCH", () => {
    it("It should error price must be a number", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/product/update/2")
          .send({
            name: "Guitar",
            price: "150000abc",
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Price must be a number");
      }
    });
  });
  describe("product/update/:id PATCH", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .patch("/product/update/1")
          .send({
            name: "Bass",
            price: 15000000,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("product/update/:id PATCH", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app).patch("/product/update/2").send({
          name: "Bass",
          price: 15000000,
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Delete Product
describe("Delete Product Test", () => {
  describe("product/delete/:id DELETE", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .delete("/product/delete/1")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Delete Product Success");
      }
    });
  });
  describe("product/delete/:id DELETE", () => {
    it("It should error product not found", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .delete("/product/delete/6")

          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Product Not Found");
      }
    });
  });

  describe("product/delete/:id DELETE", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .delete("/product/delete/1")

          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("product/delete/:id DELETE", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app).delete("/product/delete/2");

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});
