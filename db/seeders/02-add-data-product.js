"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("products", [
      {
        id: "1",
        name: "Powerbank",
        price: 123000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "2",
        name: "mmc",
        price: 300000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "3",
        name: "charger",
        price: 75000,
        createdAt: new Date(),
        updatedAt: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {},
};
