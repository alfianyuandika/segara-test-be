"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("users", [
      {
        id: "123456",
        name: "User1",
        email: "user1@gmail.com",
        password: "User1_12345",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "123457",
        name: "User2",
        email: "user2@gmail.com",
        password: "User2_12345",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "123458",
        name: "User3",
        email: "user3@gmail.com",
        password: "User3_12345",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {},
};
