"use strict";
const { Model } = require("sequelize");
const fileUpload = require("express-fileupload");
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    static associate(models) {}
  }
  product.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      price: DataTypes.DECIMAL,
      image: {
        type: DataTypes.STRING,

        get() {
          const image = this.getDataValue("image");

          if (!image) {
            return image;
          }
          return process.env.S3_URL + "/" + image;
        },
      },
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "product",
    }
  );
  return product;
};
