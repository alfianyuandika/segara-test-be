const express = require("express");
const router = express.Router();
const passport = require("passport");
const { uploadImage } = require("../middlewares/uploads/imageUpload");

const productValidator = require("../middlewares/validators/productValidator");

const productController = require("../controllers/productController");

const auth = require("../middlewares/auth");

router.get("/all", auth.admin, productController.getAllProduct);

router.get("/:id", auth.admin, productController.getOneProduct);

router.post(
  "/create",
  auth.admin,
  productValidator.create,
  uploadImage,
  productController.createProduct
);

router.delete(
  "/delete/:id",
  auth.admin,
  productValidator.delete,
  productController.deleteProduct
);

router.patch(
  "/update/:id",
  auth.admin,
  productValidator.update,
  uploadImage,
  productController.updateProduct
);

module.exports = router;
