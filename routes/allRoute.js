const express = require("express");
const router = express.Router();
const { uploadImage } = require("../middlewares/uploads/imageUpload");

const product = require("./productRoutes");
const auth = require("./authRoutes");

router.use("/product", product);
router.use("/auth", auth);

module.exports = router;
