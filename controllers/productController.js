const { product } = require("../db/models");
const Sequelize = require("sequelize");
const path = require("path");

class ProductController {
  async createProduct(req, res) {
    try {
      let createdData = await product.create({
        name: req.body.name,
        price: req.body.price,
        image: req.body.image && req.body.image,
      });

      let data = await product.findOne({
        where: {
          id: createdData.id,
        },
        attributes: ["id", "name", "price", "image", "createdAt", "updatedAt"],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOneProduct(req, res) {
    try {
      let data = await product.findOne({
        where: { id: req.params.id },
        attributes: [
          "id",
          "name",
          "price",
          "image",
          ["createdAt", "createdAt"],
        ],
      });
      if (!data) {
        return res.status(404).json({
          message: "Product Not Found",
        });
      }

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getAllProduct(req, res) {
    try {
      let data = await product.findAll({
        attributes: [
          "id",
          "name",
          "price",
          "image",
          ["createdAt", "createdAt"],
        ],
      });

      if (data.length === 0) {
        return res.status(404).json({
          message: "Product Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updateProduct(req, res) {
    let update = {
      name: req.body.name,
      price: req.body.price,
      image: req.body.image && req.body.image,
    };
    try {
      let data = await product.update(update, {
        where: { id: req.params.id },
        attributes: ["name", "price", "image"],
      });
      const name = update.name;
      const price = update.price;
      const image = update.image;

      return res.status(201).json({
        message: "Update Product Success",
        name,
        price,
        image,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async deleteProduct(req, res) {
    try {
      let data = await product.destroy({
        where: { id: req.params.id },
      });

      return res.status(201).json({
        message: "Delete Product Success",
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new ProductController();
